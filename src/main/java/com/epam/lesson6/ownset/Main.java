package com.epam.lesson6.ownset;

import java.util.Iterator;

/**
 * Created by Mikita_Kobyzau on 5/6/2016.
 */
public class Main {
    public static void main(String[] args) {
        NikHashSet<String> set = new NikHashSet<>();
        set.add("One");
        set.add("two");
        set.add("two");
        set.add("two");
        set.add("three");
        set.add("four");
        System.out.println(set.size());
        System.out.println(set.toString());
        Iterator<String> i = set.iterator();
        while (i.hasNext()){
            System.out.println("Next: " + i.next());
        }

        set.add("One1");
        set.add("two2");
        set.add("two3");
        set.add("tw4o");
        set.add("t5hree");
        set.add("f6our");
        set.add("O7ne");
        set.add("t8wo");
        set.add("t9wo");
        set.add("tw0o");
        set.add("thraee");
        set.add("fosur");
        set.add("Odne");
        set.add("tfwo");
        set.add("tgwo");
        set.add("twho");
        set.add("thjree");
        set.add("fohjur");
        System.out.println(set.size());
        System.out.println(set.toString());
         i = set.iterator();
        while (i.hasNext()){
            System.out.println("Next: " + i.next());
        }
    }
}
