package com.epam.lesson6.ownset;

import com.epam.lesson6.ownlist.NikArrayList;

import java.util.*;

/**
 * Created by Mikita_Kobyzau on 5/6/2016.
 */
public class NikHashSet<E> implements Set<E> {

    private int size;
    private Node[] table;
    private static final byte DEFAULT_CAPACITY = 10;
    private static final byte MAGNIFICATION_COEFFICIENT = 2;
    private static final byte MAX_COLLISION_COEFFICIENT = 2;
    private int modification_counter = 0;

    public NikHashSet() {
        table = new Node[DEFAULT_CAPACITY];
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        if (o == null) {
            return false;
        }
        int hash = hash(o.hashCode());
        int position = hash % (table.length - 1);
        if (table[position] != null) {
            Node node_item = table[position];
            do {
                if (node_item.hash == hash && node_item.element.equals(o)) {
                    return true;
                }
            } while ((node_item = node_item.next) != null);
        }
        return false;
    }

    @Override
    public Iterator<E> iterator() {
        return new NikSetIterator<>();
    }

    @Override
    public Object[] toArray() {
        Object[] array = new Object[size];
        int index = 0;
        Iterator<E> i = iterator();
        while (i.hasNext()){
            array[index++] = i.next();
        }
        return array;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return (T[]) toArray();
    }

    @Override
    public boolean add(E e) {
        if (e == null) {
            return false;
        }
        int hash = hash(e.hashCode());
        int position = hash % (table.length - 1);
        if (table[position] != null) {
            Node node_item = table[position];
            do {
                if (node_item.hash == hash && node_item.element.equals(e)) {
                    return false;
                }
            } while ((node_item = node_item.next) != null);
        }
        Node node = new Node(e, hash, table[position]);
        table[position] = node;
        size++;
        if (size == MAX_COLLISION_COEFFICIENT * table.length) {
            enlarge();
        }

        modification_counter++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        if (o == null) {
            return false;
        }
        int hash = hash(o.hashCode());
        int position = hash % (table.length - 1);
        if (table[position] != null) {
            Node node_item = table[position];
            if (node_item.hash == hash && node_item.element.equals(o)) {
                table[position] = node_item.next;
                modification_counter++;
                size--;
                return true;
            }
            while (node_item.next != null) {
                if (node_item.next.hash == hash && node_item.next.element.equals(o)) {
                    node_item.next = node_item.next.next;
                    modification_counter++;
                    size--;
                    return true;
                }
                node_item = node_item.next;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!contains(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        boolean isChanged = false;
        for (E e : c) {
            isChanged = add(e) || isChanged;
        }
        return isChanged;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean isChanged = false;
        for (Object o : c) {
            isChanged = remove(o) || isChanged;
        }
        return isChanged;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean isChanged = false;
        for (Object o : c) {
            isChanged = remove(o) || isChanged;
        }
        return isChanged;
    }

    @Override
    public void clear() {
        modification_counter++;
        size = 0;
        for (int i = 0; i < size; i++) {
            table[i] = null;
        }
    }

    private static int hash(int h) {
        h += (h << 15) ^ 0xffffcd7d;
        h ^= (h >>> 10);
        h += (h << 3);
        h ^= (h >>> 6);
        h += (h << 2) + (h << 14);
        h = h ^ (h >>> 16);
        return Math.abs(h);
    }

    private void enlarge() {
        modification_counter++;
        int new_capacity = Math.round(table.length * MAGNIFICATION_COEFFICIENT);
        Object[] values = toArray();
        table = new Node[new_capacity];
        size = 0;
        for (int i = 0; i < values.length; i++) {
            add((E) values[i]);
        }
    }

    private static class Node {
        private Object element;
        private int hash;
        private Node next;

        public Node(Object element, int hash, Node next) {
            this.element = element;
            this.hash = hash;
            this.next = next;
        }


    }

    private class NikSetIterator<E> implements Iterator<E> {

        private final int modification_count;

        private int index;
        private Node current_node;


        public NikSetIterator() {
            modification_count = modification_counter;
            index = 0;
            current_node = null;
        }

        @Override
        public boolean hasNext() {
            Node node = current_node;
            int index = this.index;
            if (node != null) {
                node = node.next;
            }
            while (node == null && table.length != index) {
                node = table[index++];
            }
            return node != null;
        }

        @Override
        public E next() {
            if (modification_counter != modification_count) {
                throw new ConcurrentModificationException();
            }
            if (current_node != null) {
                current_node = current_node.next;
            }
            while (current_node == null && table.length != index) {
                current_node = table[index++];
            }
            if (current_node == null) {
                throw new NoSuchElementException();
            }
            return (E) current_node.element;
        }

    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < table.length; i++) {
            sb.append(i + ": (");
            Node n = table[i];
            while (n != null) {
                sb.append(n.element);
                sb.append("|");
                n = n.next;
            }
            sb.append(") ");
        }

        return sb.toString();
    }
}
