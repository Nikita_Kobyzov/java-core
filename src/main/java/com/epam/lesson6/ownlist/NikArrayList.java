package com.epam.lesson6.ownlist;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;



public class NikArrayList<E> extends AbstractList<E> implements List<E>, Cloneable, RandomAccess {

    private Object[] array;
    private int size;
    private static final int DEFAULT_CAPACITY = 16;
    private static final Object[] EMPTY_ARRAY = {};
    private static final float MAGNIFICATION_COEFFICIENT = 1.5F;

    public NikArrayList() {
        this(DEFAULT_CAPACITY);
    }

    public NikArrayList(Collection<? super E> collection) {
        this(collection.toArray());
    }

    public NikArrayList(int capacity) {
        size = 0;
        if (capacity > 0) {
            array = new Object[capacity];
        } else if (capacity == 0) {
            array = EMPTY_ARRAY;
        } else if (capacity < 0) {
            throw new IllegalArgumentException("Illegal Capacity: " +
                    capacity);
        }
    }

    public NikArrayList(Object[] array) {
        if (array.length != 0) {
            this.array = array;
            size = array.length;
        } else if (array.length == 0) {
            this.array = EMPTY_ARRAY;
            size = 0;
        }
    }


    @Override
    public E set(int index, E element) {
        checkRange(index);
        E el = (E) array[index - 1];
        array[index - 1] = element;
        return el;
    }

    @Override
    public void add(int index, E element) {
        if (size != array.length) {
            for (int i = size; i >= index; i--) {
                array[i] = array[i - 1];
                break;
            }
            array[index - 1] = element;
            size++;
        } else {
            enlarge();
            add(index, element);
        }
    }

    @Override
    public boolean add(E e) {
        add(size+1, e);
        return true;
    }


    public void trimToSize() {
       array = deleteNulls(array, size);
    }

    public int capacity(){
        return array.length;
    }
    @Override
    public E remove(int index) {
        checkRange(index);
        E element = (E) array[index - 1];
        array[index - 1] = null;
        array = deleteNulls(array, array.length);
        size--;
        return element;
    }

    @Override
    public E get(int index) {
        checkRange(index);
        return (E) array[index - 1];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void replaceAll(UnaryOperator<E> operator) {
        for (int i = 0; i < size; i++) {
            array[i] = operator.apply((E) array[i]);
        }
    }


    @Override
    public void forEach(Consumer<? super E> action) {
        for(int i = 0; i< size; i++){
            action.accept((E)array[i]);
        }
    }

    @Override
    public boolean removeIf(Predicate<? super E> filter) {
        int removed_count = 0;
        for (int i = 0; i < size; i++) {
            if (filter.test((E) array[i])) {
                array[i] = null;
                removed_count++;
            }
        }
        array = deleteNulls(array, array.length);
        size -=removed_count;
        return removed_count != 0;
    }

    private void checkRange(int index) {
        if (index < 0 || index > size())
            throw new IndexOutOfBoundsException("Size: " + size + ", index: " + index);
    }


    private void enlarge() {
        int new_capacity = Math.round(array.length * MAGNIFICATION_COEFFICIENT);
        Object[] new_array = new Object[new_capacity];
        for (int i = 0; i < array.length; i++) {
            new_array[i] = array[i];
        }
        array = new_array;
    }

    private Object[] deleteNulls(Object[] array, int capacity) {
        Object[] new_array = new Object[capacity];
        int index = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != null) {
                new_array[index++] = array[i];
            }
        }
        return new_array;
    }


    @Override
    public Object[] toArray() {
        return deleteNulls(array, size);


       }
}
