package com.epam.lesson6.ownlist;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by Mikita_Kobyzau on 5/3/2016.
 */
public class Main {
    public static void main(String[] args) {
       adding();
    }


    public static void adding(){
        NikArrayList<Integer> test = new NikArrayList<>();
        ArrayList<Integer> arrayList = new ArrayList<>();
        LinkedList<Integer> linkedList = new LinkedList<>();
        long start, end, count = 1000000;
        start = System.currentTimeMillis();
        for(int i = 1; i<count; i++){
            test.add(new Integer(i));
        }
        end = System.currentTimeMillis();
        System.out.println("MY " + (end - start));

        start = System.currentTimeMillis();
        for(int i = 1; i<count; i++){
            arrayList.add(new Integer(i));
        }
        end = System.currentTimeMillis();
        System.out.println(end - start);

        start = System.currentTimeMillis();
        for(int i = 1; i<count; i++){
            linkedList.add(new Integer(i));
        }
        end = System.currentTimeMillis();
        System.out.println(end - start);




        System.exit(0);
    }

    public static void getting(){
        NikArrayList<Integer> test = new NikArrayList<>();
        ArrayList<Integer> arrayList = new ArrayList<>();
        LinkedList<Integer> linkedList = new LinkedList<>();
        long start, end, count = 100000;

        for(int i = 1; i<count; i++){
            test.add(new Integer(i));
        }
        for(int i = 1; i<count; i++){
            arrayList.add(new Integer(i));
        }
        for(int i = 1; i<count; i++){
            linkedList.add(new Integer(i));
        }


        start = System.currentTimeMillis();
        for(int i = 1; i<count-1; i++){
            test.get(i);
        }
        end = System.currentTimeMillis();
        System.out.println("MY " + (end - start));

        start = System.currentTimeMillis();
        for(int i = 1; i<count-1; i++){
            arrayList.get(i);
        }
        end = System.currentTimeMillis();
        System.out.println(end - start);

        start = System.currentTimeMillis();
        for(int i = 1; i<count-1; i++){
            linkedList.get(i);
        }
        end = System.currentTimeMillis();
        System.out.println(end - start);




        System.exit(0);
    }

    public static void testing(){
        NikArrayList<Integer> test = new NikArrayList<>();

        for(int i = 1; i<11; i++){
            test.add(new Integer(i));
        }

        NikArrayList<Integer> test2 = new NikArrayList<>(test);
        test2.forEach(System.out::println);

        test.forEach(System.out::println);
        System.out.println("***************");
        test.remove(5);
        test.forEach(System.out::println);
        System.out.println("***************");
        test.add(new Integer(40));
        test.add(5,new Integer(40));
        test.set(7,new Integer(40));
        test.forEach(System.out::println);
        System.out.println("***************");
        test.removeIf(new Integer(40)::equals);
        test.forEach(System.out::println);
        System.out.println("***************");
        System.out.println(test.capacity());
        test.trimToSize();
        System.out.println(test.capacity());
    }
}
