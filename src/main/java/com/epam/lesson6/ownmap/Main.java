package com.epam.lesson6.ownmap;


/**
 * Created by Mikita_Kobyzau on 5/12/2016.
 */
public class Main {
    public static void main(String[] args) {
        NikHashMap<Integer, String> map = new NikHashMap<>();
        map.put(1, "One");
        map.put(2, "two");
        map.put(2, "two");
        map.put(2, "two");
        map.put(3, "three");
        map.put(4, "four");
        map.put(null, "null");
        System.out.println(map.size());
        System.out.println(map.get(2));
        System.out.println(map.containsKey(null));
        System.out.println(map.containsKey(4));
        System.out.println(map.containsKey(6));

        System.out.println(map.values().size());
        System.out.println(map.keySet());
        System.out.println(map.entrySet().size());
    }
}
