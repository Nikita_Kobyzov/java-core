package com.epam.lesson6.ownmap;

import com.epam.lesson6.ownlist.NikArrayList;
import com.epam.lesson6.ownset.NikHashSet;

import java.util.*;

/**
 * Created by Mikita_Kobyzau on 5/12/2016.
 */
public class NikHashMap<K, V> implements Map<K, V> {

    private int size;
    private Node<K, V>[] table;
    private static final byte DEFAULT_CAPACITY = 10;
    private static final byte MAGNIFICATION_COEFFICIENT = 2;
    private static final byte MAX_COLLISION_COEFFICIENT = 2;

    public NikHashMap() {
        table = new Node[DEFAULT_CAPACITY];
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        if (key == null) {
            return hasNullKey();
        }
        int hash = hash(key.hashCode());
        int position = hash % (table.length - 1);
        if (table[position] != null) {
            Node node_item = table[position];
            do {
                if (node_item.hash == hash && node_item.key.equals(key)) {
                    return true;
                }
            } while ((node_item = node_item.next) != null);
        }
        return false;
    }


    @Override
    public boolean containsValue(Object value) {
        for (int i = 0; i < table.length; i++) {
            Node node = table[i];
            while (node != null) {
                if (node.value.equals(value)) {
                    return true;
                }
                node = node.next;
            }
        }
        return false;
    }

    @Override
    public V get(Object key) {
        if (key == null) {
            if (hasNullKey()) {
                return getNullKeyValue();
            }
            return null;
        }
        int hash = hash(key.hashCode());
        int position = hash % (table.length - 1);
        if (table[position] != null) {
            Node<K, V> node_item = table[position];
            do {
                if (node_item.hash == hash && node_item.key.equals(key)) {
                    return node_item.value;
                }
            } while ((node_item = node_item.next) != null);
        }
        return null;
    }


    @Override
    public V put(K key, V value) {
        if (key == null) {
            if (hasNullKey()) {
                V old = getNullKeyValue();
                addNullKey(value);
                return old;
            }
            addNullKey(value);
            size++;
            return null;
        }
        int hash = hash(key.hashCode());
        int position = hash % (table.length - 1);
        if (table[position] != null) {
            Node<K, V> node_item = table[position];
            do {
                if (node_item.hash == hash && node_item.key.equals(key)) {
                    V old = node_item.value;
                    node_item.value = value;
                    return old;
                }
            } while ((node_item = node_item.next) != null);
        }
        Node node = new Node(key, value, hash, table[position]);
        table[position] = node;
        size++;
        if (size == MAX_COLLISION_COEFFICIENT * table.length) {
            enlarge();
        }
        return null;
    }

    @Override
    public V remove(Object key) {
        if (key == null) {
            if (hasNullKey()) {
                V value = getNullKeyValue();
                removeNullValue();
                return value;
            }
            return null;
        }
        int hash = hash(key.hashCode());
        int position = hash % (table.length - 1);
        if (table[position] != null) {
            Node<K, V> node_item = table[position];
            if (node_item.hash == hash && node_item.key.equals(key)) {
                V old = table[position].value;
                table[position] = node_item.next;
                size--;
                return old;
            }
            while (node_item.next != null) {
                if (node_item.next.hash == hash && node_item.next.key.equals(key)) {
                    V old = node_item.next.value;
                    node_item.next = node_item.next.next;
                    size--;
                    return old;
                }
                node_item = node_item.next;
            }
        }
        return null;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        Set set = m.entrySet();
        Iterator<Entry> i = set.iterator();
        while (i.hasNext()) {
            Entry<K, V> entry = i.next();
            put(entry.getKey(), entry.getValue());
        }

    }

    @Override
    public void clear() {
        size = 0;
        for (int i = 0; i < size; i++) {
            table[i] = null;
        }
    }

    @Override
    public Set<K> keySet() {
        NikHashSet<K> keySet = new NikHashSet<>();
        for (int i = 0; i < table.length; i++) {
            Node<K,V> node = table[i];
            while (node != null) {
               keySet.add(node.key);
                node = node.next;
            }
        }
        return keySet;
    }

    @Override
    public Collection<V> values() {
        NikArrayList<V> list = new NikArrayList<>(size);
        for (int i = 0; i < table.length; i++) {
            Node<K,V> node = table[i];
            while (node != null) {
                list.add(node.value);
                node = node.next;
            }
        }
        return list;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        NikHashSet<Entry<K, V>> entrySet = new NikHashSet<>();
        for (int i = 0; i < table.length; i++) {
            Node<K,V> node = table[i];
            while (node != null) {
                entrySet.add(node);
                node = node.next;
            }
        }
        return entrySet;
    }

    private static class Node<K, V> implements Entry<K, V> {
        private K key;
        private V value;
        private int hash;
        private Node<K, V> next;

        public Node() {
        }

        public Node(K key, V value, int hash, Node next) {
            this.key = key;
            this.value = value;
            this.hash = hash;
            this.next = next;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            V old = this.value;
            this.value = value;
            return old;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Node<?, ?> node = (Node<?, ?>) o;

            return key != null ? key.equals(node.key) : node.key == null;

        }

        @Override
        public int hashCode() {
            return hash;
        }
    }

    private void enlarge() {
        int new_capacity = Math.round(table.length * MAGNIFICATION_COEFFICIENT);
        Set<Entry<K, V>> set = entrySet();
        table = new Node[new_capacity];
        size = 0;
        Iterator<Entry<K, V>> i = set.iterator();
        while (i.hasNext()) {
            Entry<K, V> entry = i.next();
            put(entry.getKey(), entry.getValue());
        }
    }

    private static int hash(int h) {
        h += (h << 15) ^ 0xffffcd7d;
        h ^= (h >>> 10);
        h += (h << 3);
        h ^= (h >>> 6);
        h += (h << 2) + (h << 14);
        h = h ^ (h >>> 16);
        return Math.abs(h);
    }

    private boolean hasNullKey() {
        Node node = table[0];
        while (node != null) {
            if (node.key == null) {
                return true;
            }
        }
        return false;
    }

    private V removeNullValue() {
        Node<K, V> node = table[0];
        while (node != null) {
            if (node.key == null) {
                return node.value;
            }
        }
        return null;
    }

    private V getNullKeyValue() {
        Node<K, V> node = table[0];
        while (node != null) {
            if (node.key == null) {
                return node.value;
            }
        }
        throw new NoSuchElementException("Element with null key is not found");
    }

    private void addNullKey(V value) {
        Node<K, V> node = new Node<>();
        node.key = null;
        node.hash = 0;
        node.value = value;
        node.next = table[0];
        table[0] = node;
    }

}
