package com.epam.lesson1.init_reflection;

/**
 * Created by Mikita_Kobyzau on 3/28/2016.
 */
public class OuterClass1 {
    {
        System.out.println("c in block1: " + this.c); //5
    }
    static {
        System.out.println("Static");
    }
    private int a = 1;
    private static int b = 2;
    private final int c =5;
    {
        a=3;
    }
    static {
        b = 4;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public static int getB() {
        return b;
    }

    public static void setB(int b) {
        OuterClass1.b = b;
    }
}
