package com.epam.lesson1.serializ;

import java.io.Serializable;

/**
 * Created by Mikita_Kobyzau on 3/28/2016.
 */
public class Bean implements Serializable {
    private int a=1;
    private static int b = 2;
    static {
        System.out.println("Static block " + b);
        b = 4;
        System.out.println("Static block " + b);
    }
    {
        System.out.println("Logic block " + a);
        a = 5;
        System.out.println("Logic block " + a);

    }

    public Bean(){
        a = 6;
    }
    @Override
    public String toString() {
        return "Bean{" +
                "a=" + a +
                '}';
    }
}
