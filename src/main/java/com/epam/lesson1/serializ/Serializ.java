package com.epam.lesson1.serializ;

import java.io.FileOutputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

/**
 * Created by Mikita_Kobyzau on 3/28/2016.
 */
public class Serializ {
    public static void main(String args[]) throws Exception{
        FileOutputStream fos = new FileOutputStream("file.txt");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(new Bean());
        fos.flush();
    }
}
