package com.epam.lesson1.anonymous;

/**
 * Created by Mikita_Kobyzau on 3/31/2016.
 */
public class Main {

    public static void main(String args[]) throws Exception{
        Main main = new Main(){
            public void say(){
                System.out.println("Two");
            }
        };
        System.out.println(main.getClass().getName());
        Class aa = Class.forName("com.epam.lesson1.anonymous.Main$1");
        Main a = (Main)aa.newInstance();
        a.say();
    }

    public void say(){
        System.out.println("Main");
    }
}
