package com.epam.lesson2.lose_accuracy;

/**
 * Created by Mikita_Kobyzau on 3/30/2016.
 */
public class LoseAccuracy01 {
    public static void main(String[] args) {
        int x1 = 123456788;
        int x2 = 10010101;
        float f1 = x1;
        float f2 = x2;
        System.out.println("f1 - "+f1);
        System.out.println("f2 - "+f2);
        long l1 = 10000000000000001L;
        double d1 = l1;
        System.out.println(l1);
        System.out.println(d1);
    }
}
