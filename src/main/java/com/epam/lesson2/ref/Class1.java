package com.epam.lesson2.ref;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mikita_Kobyzau on 3/31/2016.
 */
public class Class1 {
    public static void main(String args[]){
        Class1 c = new Class1(){
            @Override
            public String toString() {
                return "he";
            }
        };
        Inner1 xx=new Inner1();
        List<Inner1> l = new ArrayList<Inner1>();
        l.add(new Inner1());
        l.forEach((i)->System.out.println(m(i)));
        long x = 5463454466346L;
        byte t = (byte) x;
        System.out.print(c.getClass().getName() + " " + t);
    }
    public static Inner1 m(Inner1 a){
        a = new Inner1();
        return a;
    }
    static class Inner1{

    }
}
