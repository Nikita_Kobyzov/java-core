package com.epam.lesson2.static_content;

/**
 * Created by Mikita_Kobyzau on 3/31/2016.
 */
public class Class1 {
    private int x = 1;

    private static int y = 2;

    {
        class Local1 {
            int z = 2;

            private void d() {
                x = 4;

                class Local2 {

                    void dd() {
                        x = 4;
                        y = 5;
                        z = 5;
                        d();
                    }
                }
            }
        }
    }


    {
        StringBuffer x = new StringBuffer();
        x.append(1);
        class AA {
            StringBuffer method( StringBuffer a) {
                a.append(5);
                return a;
            }
        }
        new AA(){
            @Override
            StringBuffer method(StringBuffer a) {
                a = new StringBuffer();
                return super.method(a);
            }
        };

    }
}
