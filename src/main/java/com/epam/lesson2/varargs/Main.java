package com.epam.lesson2.varargs;

/**
 * Created by Mikita_Kobyzau on 3/31/2016.
 */
public class Main {
    static void say(int x1, int x2){
        System.out.println("int x int x");
    }
    static void say(long x1, long x2){
        System.out.println("long x long x");
    }

    static void say(long ... x){
        System.out.println("long ...");
    }
    static void say(int ... x){
        System.out.println("int ...");
    }

    static void doIt(Inner1 i){
        System.out.println("Inner1");
    }

    static void doIt(Inner2 i){
        System.out.println("Inner2");
    }
    static void doIt(Inner1 ... i){
        System.out.println("Inner1 ... ");
    }

    static void doIt(Inner2... i){
        System.out.println("Inner2 ...");
    }


    public static void main(String args[]){
        say(1,2,6);
        Inner1 i = new Inner2();
        doIt(i,i);
        Main[] ff = new Main[]{new Main(),new Main()};
        System.out.println(ff.getClass().getName());
    }

    static class Inner1{

    }
    static class Inner2 extends Inner1{

    }
}
