package com.epam.lesson2.enums;


public enum Enum1 {
    ONE("One"){
        void say(){
            System.out.println("Anon say");
            throw new RuntimeException("Hello");
        }
    };
    Enum1(String s){
        System.out.println("Constructor " + s);
    }

     static {
        System.out.println("Static");
        d();

    }

    public static void d(){

        Integer a =400;
        Integer b =500;
        a +=b;
        Integer c = 900;
        System.out.println(a +" "+ b + " " + (a==c));
        //throw new RuntimeException("WHYYYYY");
    }
    {
        System.out.println("Logic");
        Inner1 i = new Inner1();
    }

      static class Inner1{
        public  void ss(){

            throw new RuntimeException("Hello");
        }
    }

}

