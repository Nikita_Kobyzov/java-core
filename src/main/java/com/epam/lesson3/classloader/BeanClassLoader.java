package com.epam.lesson3.classloader;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;


public class BeanClassLoader extends ClassLoader {

    private String className;
    private Map<String, Class<?>> cache = new HashMap<>();
    private JarFile jarFile;
    private final ClassLoader parent;

    public BeanClassLoader(ClassLoader parent, String jarFileName, String className) throws IOException {
        this.parent = parent;
        this.jarFile = new JarFile(jarFileName);
        this.className = className;
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        Class<?> result = cache.get(name);
        if (result != null) {
            System.out.println(className+ ":" + "Class " + name + " found in cache");
            return result;
        }
        System.out.println(className+ ":" + "Class " + name + " not found in cache");
        try {
            return parent.loadClass(name);
        } catch (ClassNotFoundException e) {
            System.out.println(className+ ":" + "Parent Class loader does not found " + name + " in cache");
            try {
                result = findInJar(name);
            } catch (IOException me) {
                me.printStackTrace();
            }
            if (result == null) {
                throw new ClassNotFoundException(className+ ":" + "Class " + name + " not found", e);
            }
        }


        return result;
    }

    private Class<?> findInJar(String className) throws IOException {
        String systemClassName = className.replace('.', '/');
        Enumeration<JarEntry> jarEntries = jarFile.entries();
        while (jarEntries.hasMoreElements()) {
            JarEntry jarEntry = jarEntries.nextElement();
            if (jarEntry.isDirectory())
                continue;
            if (jarEntry.getName().endsWith(".class")) {
                if (!jarEntry.getName().substring(0, jarEntry.getName().length() - 6).equals(systemClassName)) {
                    continue;
                }
                byte[] classData = loadClassData(jarFile, jarEntry);

                if (classData != null) {
                    Class<?> defClass = defineClass(
                            jarEntry.getName().replace('/', '.').substring(0, jarEntry.getName().length() - 6),
                            classData, 0, classData.length);
                    cache.put(defClass.getName(), defClass);
                    System.out.println(this.className+ ":" + "Put in cache: " + defClass.getName());
                    return defClass;
                }
            }
        }
        return null;
    }

    private byte[] loadClassData(JarFile jarFile, JarEntry jarEntry)
            throws IOException {
        long size = jarEntry.getSize();
        if (size <= 0)
            return null;
        else if (size > Integer.MAX_VALUE) {
            throw new IOException("Class size too long");
        }
        byte[] buffer = new byte[(int) size];
        InputStream is = jarFile.getInputStream(jarEntry);
        is.read(buffer);

        return buffer;
    }

}
