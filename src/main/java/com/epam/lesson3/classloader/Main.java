package com.epam.lesson3.classloader;

/**
 * Created by Mikita_Kobyzau on 4/6/2016.
 */
public class Main {
    public static void main(String args[]) throws Exception{
        BeanClassLoader beanClassLoader = new BeanClassLoader(Main.class.getClassLoader(), args[0], "1");
        BeanClassLoader beanClassLoader2 = new BeanClassLoader(beanClassLoader, args[0], "2");
        for (int i = 1; i<args.length;i++){
            System.out.println("****** " + beanClassLoader2.loadClass(args[i]).getName());
        }


    }
}
