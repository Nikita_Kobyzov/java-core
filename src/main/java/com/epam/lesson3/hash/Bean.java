package com.epam.lesson3.hash;

/**
 * Created by Mikita_Kobyzau on 4/6/2016.
 */
public class Bean {
    private int x;
    private int y;

    public Bean(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bean bean = (Bean) o;

        if (x != bean.x) return false;
        return y == bean.y;

    }

    @Override
    public int hashCode() {
        System.out.println(1);
        int result = x;
        result = 31 * result + y;
        return result;
    }
}
