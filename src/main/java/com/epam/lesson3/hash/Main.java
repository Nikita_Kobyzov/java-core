package com.epam.lesson3.hash;

/**
 * Created by Mikita_Kobyzau on 4/6/2016.
 */
public class Main {
    public static void main(String args[]){
        Bean bean = new Bean(1,2);
        System.out.println(bean.hashCode());
        System.out.println(bean.hashCode());
        System.out.println(bean.hashCode());
        System.out.println(bean.hashCode());

        Object object = new Bean(1,2);
        System.out.println(object.hashCode());
        System.out.println(object.hashCode());
        System.out.println(object.hashCode());
        System.out.println(object.hashCode());

    }
}
