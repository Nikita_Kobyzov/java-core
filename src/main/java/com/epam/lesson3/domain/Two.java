package com.epam.lesson3.domain;

import com.epam.lesson3.bean.Four;
import com.epam.lesson3.bean.Three;

/**
 * Created by Mikita_Kobyzau on 4/5/2016.
 */
public class Two extends Three {

    private One one = new One();
    private Four four;

    public One getOne() {
        return one;
    }

    public void setOne(One one) {
        this.one = one;
    }

    public Four getFour() {
        return four;
    }

    public void setFour(Four four) {
        this.four = four;
    }
}
