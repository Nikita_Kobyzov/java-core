package com.epam.lesson3.bean;

/**
 * Created by Mikita_Kobyzau on 4/5/2016.
 */
public class Three {
    static {
        System.out.println("Static Three!");
        Four.x = 5;
        System.out.println("Static Three!");
    }
}
