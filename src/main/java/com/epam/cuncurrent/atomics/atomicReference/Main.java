package com.epam.cuncurrent.atomics.atomicReference;

import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by Mikita_Kobyzau on 5/4/2016.
 */
public class Main {

    private volatile static AtomicReference<Integer> atomicReference = new AtomicReference<>();

    public static void main(String[] args) throws Exception {

        atomicReference.set(1);
        Thread t1 = new Thread(new MyThread(5));
        Thread t2 = new Thread(new MyThread(4));
        Thread t3 = new Thread(new MyThread(3));
        Thread t4 = new Thread(new MyThread(2));
        Thread t5 = new Thread(new MyThread(1));
        Thread t6 = new Thread(new MyThread(5));
        Thread t7 = new Thread(new MyThread(4));
        Thread t8 = new Thread(new MyThread(3));
        Thread t9 = new Thread(new MyThread(2));
        Thread t10 = new Thread(new MyThread(1));
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();
        t7.start();
        t8.start();
        t9.start();
        t10.start();
        t1.join();
        t2.join();
        t3.join();
        t4.join();
        t5.join();
        t6.join();
        t7.join();
        t8.join();
        t9.join();
        t10.join();
        System.out.println(atomicReference.get());
    }


    public static class MyThread implements Runnable {
        private int value;

        public MyThread(int x) {
            value = x;
        }

        @Override
        public void run() {
            boolean b = false;
            try {
                while (!b) {
                    Thread.sleep(100);
                    b = atomicReference.compareAndSet(value, value + 1);
                    System.out.println(Thread.currentThread().getName() + b);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
