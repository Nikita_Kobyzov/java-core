package com.epam.cuncurrent.atomics.atomicReferenceArray;

import java.util.concurrent.atomic.AtomicReferenceArray;

/**
 * Created by Mikita_Kobyzau on 5/4/2016.
 */
public class Main {

    private volatile  static AtomicReferenceArray<String> ara= new AtomicReferenceArray<String>(10);
    private volatile static String[] noa = {null, "ref2"};
    private  static class AddThread implements Runnable{
        @Override
        public void run() {
            boolean b = false;
            while (!b){
              /*  if(noa[0]==null){
                    noa[0] = "*****";
                    b = true;
                }
                */
                b = ara.compareAndSet(0, null,"*****");
            }
            System.out.println("ADD");
        }

    }

    private  static class RemoveThread implements Runnable{
        @Override
        public void run() {
            boolean b = false;
            while (!b){
                /*
                if(noa[0]=="*****"){
                    noa[0] = null;
                    b = true;
                }
                */
                b = ara.compareAndSet(0, "*****",null);
            }
            System.out.println("REMOVE");
        }

    }

    public static void main(String[] args){
        new Thread(new AddThread()).start();
        new Thread(new AddThread()).start();
        new Thread(new AddThread()).start();
        new Thread(new AddThread()).start();
        new Thread(new AddThread()).start();
        new Thread(new AddThread()).start();
        new Thread(new AddThread()).start();
        new Thread(new AddThread()).start();
        new Thread(new AddThread()).start();
        new Thread(new AddThread()).start();
        new Thread(new AddThread()).start();
        new Thread(new AddThread()).start();
        new Thread(new AddThread()).start();
        new Thread(new AddThread()).start();
        new Thread(new AddThread()).start();
        new Thread(new AddThread()).start();
        new Thread(new AddThread()).start();
        new Thread(new AddThread()).start();
        new Thread(new AddThread()).start();
        new Thread(new AddThread()).start();

        new Thread(new RemoveThread()).start();
        new Thread(new RemoveThread()).start();
        new Thread(new RemoveThread()).start();
        new Thread(new RemoveThread()).start();
        new Thread(new RemoveThread()).start();
        new Thread(new RemoveThread()).start();
        new Thread(new RemoveThread()).start();
        new Thread(new RemoveThread()).start();
        new Thread(new RemoveThread()).start();
        new Thread(new RemoveThread()).start();
        new Thread(new RemoveThread()).start();
        new Thread(new RemoveThread()).start();
        new Thread(new RemoveThread()).start();
        new Thread(new RemoveThread()).start();
        new Thread(new RemoveThread()).start();
        new Thread(new RemoveThread()).start();
        new Thread(new RemoveThread()).start();
        new Thread(new RemoveThread()).start();
        new Thread(new RemoveThread()).start();
        new Thread(new RemoveThread()).start();


         }
}
