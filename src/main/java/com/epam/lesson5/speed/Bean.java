package com.epam.lesson5.speed;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Mikita_Kobyzau on 4/28/2016.
 */
public class Bean {
    private int x1 = 0;
    private AtomicInteger x2 = new AtomicInteger(0);

    public  synchronized void  incS(){
        x1++;
    }
    public void incA(){
        x2.incrementAndGet();
    }

    @Override
    public String toString() {
        return "" + x1 + " " +x2.get();
    }
}
