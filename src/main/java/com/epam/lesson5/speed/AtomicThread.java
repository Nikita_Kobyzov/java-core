package com.epam.lesson5.speed;


public class AtomicThread extends Thread{
    private Bean b;

    public AtomicThread(Bean i) {
        this.b = i;
    }

    @Override
    public void run() {
        for(int i = 0; i<100000;i++){
            b.incA();
        }
    }
}
