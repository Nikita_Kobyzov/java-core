package com.epam.lesson5.speed;

/**
 * Created by Mikita_Kobyzau on 4/28/2016.
 */
public class Main {

    public static void main(String args[]) throws Exception {
        Bean b = new Bean();
        AtomicThread[] a = {new AtomicThread(b), new AtomicThread(b), new AtomicThread(b), new AtomicThread(b), new AtomicThread(b)};

        SyncThread[] s = {new SyncThread(b), new SyncThread(b), new SyncThread(b), new SyncThread(b), new SyncThread(b)};

        long start = System.currentTimeMillis();
        for (int i = 0; i < a.length; i++) {
            a[i].start();
        }
        for (int i = 0; i < a.length; i++) {
            a[i].join();
        }
        long end = System.currentTimeMillis();

        System.out.println(end - start);

        start = System.currentTimeMillis();
        for (int i = 0; i < s.length; i++) {
            s[i].start();
        }
        for (int i = 0; i < s.length; i++) {
            s[i].join();
        }
        end = System.currentTimeMillis();

        System.out.println(end - start);

        System.out.println(b.toString());

    }
}
