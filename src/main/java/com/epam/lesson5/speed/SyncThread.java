package com.epam.lesson5.speed;

/**
 * Created by Mikita_Kobyzau on 4/28/2016.
 */
public class SyncThread extends Thread {
    private Bean bean;

    public SyncThread(Bean bean) {
        this.bean = bean;
    }

    @Override
    public void run() {
        for(int i = 0; i<100000;i++){
            bean.incS();
        }
    }
}
