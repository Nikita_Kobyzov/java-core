package com.epam.lesson5.exeptions;

/**
 * Created by Mikita_Kobyzau on 4/27/2016.
 */
public class IllegalArgumentExceptionExample extends Thread {
    public IllegalArgumentExceptionExample(String name) {
        super(name);
    }

    @Override
    public void run() {
        try{
            Thread.sleep(-50);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
