package com.epam.lesson5.exeptions;

/**
 * Created by Mikita_Kobyzau on 4/27/2016.
 */
public class InterruptedExceptionExample extends Thread {
    public InterruptedExceptionExample(String name) {
        super(name);
    }

    @Override
    public void run() {
        Thread t = new Thread(()->{
            try{
                Thread.sleep(10000);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        });
        t.start();
        try{
            Thread.sleep(500);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        t.interrupt();
    }
}
