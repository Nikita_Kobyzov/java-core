package com.epam.lesson5.exeptions;

/**
 * Created by Mikita_Kobyzau on 4/27/2016.
 */
public class IllegalThreadStateExceptionExample extends Thread {
    public IllegalThreadStateExceptionExample(String name) {
        super(name);
    }

    @Override
    public void run() {
        this.setDaemon(true);
    }
}
