package com.epam.lesson5.exeptions;

/**
 * Created by Mikita_Kobyzau on 4/27/2016.
 */
public class NoSuchMethodErrorExample extends Thread {
    public NoSuchMethodErrorExample(String name) {
        super(name);
    }

    @Override
    public void run() {
        this.destroy();
    }
}
