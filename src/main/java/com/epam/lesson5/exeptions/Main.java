package com.epam.lesson5.exeptions;

/**
 * Created by Mikita_Kobyzau on 4/27/2016.
 */
public class Main {

    public static void main(String args[]) throws Exception{
        InterruptedExceptionExample interruptedExceptionExample = new InterruptedExceptionExample("InterruptedExceptionExample");
        IllegalArgumentExceptionExample illegalArgumentExceptionExample = new IllegalArgumentExceptionExample("IllegalArgumentExceptionExample");
        IllegalThreadStateExceptionExample illegalThreadStateExceptionExample = new IllegalThreadStateExceptionExample("IllegalThreadStateExceptionExample");
        NoSuchMethodErrorExample noSuchMethodErrorExample = new NoSuchMethodErrorExample("NoSuchMethodErrorExample");
        interruptedExceptionExample.start();
        interruptedExceptionExample.join();
        illegalArgumentExceptionExample.start();
        illegalArgumentExceptionExample.join();
        illegalThreadStateExceptionExample.start();
        illegalThreadStateExceptionExample.join();
        noSuchMethodErrorExample.start();
        noSuchMethodErrorExample.join();
    }
}
