package com.epam.lesson5.callable;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by Mikita_Kobyzau on 4/27/2016.
 */
public class Main {
    public static void main(String args[]) throws Exception{
        ExecutorService service = Executors.newCachedThreadPool();
        MyCall myCall = new MyCall();
        Future<Integer> future = service.submit(myCall);
        System.out.println(future.get());
    }
}
