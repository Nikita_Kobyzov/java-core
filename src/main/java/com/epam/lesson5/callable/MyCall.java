package com.epam.lesson5.callable;

import java.util.concurrent.Callable;

/**
 * Created by Mikita_Kobyzau on 4/27/2016.
 */
public class MyCall implements Callable<Integer> {

    @Override
    public Integer call() throws Exception {
        Thread.sleep(3000);
        return 42;
    }
}
