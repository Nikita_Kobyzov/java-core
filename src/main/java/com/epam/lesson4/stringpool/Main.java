package com.epam.lesson4.stringpool;

/**
 * Created by Mikita_Kobyzau on 4/19/2016.
 */
public class Main {
    
    public static void main(String[] args){
        Integer l1 = 1;
        String s1  = "1";
        String s2 = String.valueOf(1);
        String s3 = l1.toString();
        System.out.println(s1 + s2 +s3);
        System.out.println("s1==s2 " + (s1==s2));
        System.out.println("s1==s3 " + (s1==s3));
        System.out.println("s2==s3 " + (s2==s3));


        String a1 = "abc";
        String a2 = new String("abc");
        String a3 = a2;
        System.out.println((a1==a2));
        a2.intern();
        System.out.println((a3==a2));
    }
}
