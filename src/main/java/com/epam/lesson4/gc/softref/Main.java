package com.epam.lesson4.gc.softref;

import com.epam.lesson4.gc.Collector;

import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;

/**
 * Created by Mikita_Kobyzau on 4/12/2016.
 */
public class Main {
    public static void main(String[] args){
        Bean[] beans = {new Bean(), new Bean(), new Bean()};
        Collector collector = new Collector();
        WeakReference<Bean> b1 = new WeakReference<Bean>(beans[0]);
        WeakReference<Bean> b2 = new WeakReference<Bean>(beans[1]);
        WeakReference<Bean> b3 = new WeakReference<Bean>(beans[2]);
        System.out.println("1:before " + b1.get() + ", " + b2.get() + ", "+ b3.get());
        for(int i = 0; i<10;i++) collector.addMemory();
        System.out.println("1:after " + b1.get() + ", " + b2.get() + ", "+ b3.get());
        beans[0] = null;
        System.out.println("2:before " + b1.get() + ", " + b2.get() + ", "+ b3.get());
        for(int i = 0; i<10;i++) collector.addMemory();
        System.out.println("2:after " + b1.get() + ", " + b2.get() + ", "+ b3.get());
        beans[1] = null;
        System.out.println("3:before " + b1.get() + ", " + b2.get() + ", "+ b3.get());
        for(int i = 0; i<10;i++) collector.addMemory();
        System.out.println("3:after " + b1.get() + ", " + b2.get() + ", "+ b3.get());
        beans[2] = null;
        System.out.println("4:before " + b1.get() + ", " + b2.get() + ", "+ b3.get());
        for(int i = 0; i<50;i++) collector.addMemory();
        System.out.println("4:after " + b1.get() + ", " + b2.get() + ", "+ b3.get());
    }
}
