package com.epam.lesson4.gc;

import java.lang.ref.WeakReference;

/**
 * Created by Mikita_Kobyzau on 4/20/2016.
 */
public class RefMain {
    public static void main(String args[]){
        RefBean f = new RefBean();
        WeakReference<RefBean> w = new WeakReference<RefBean>(f);
        f  = null;

        System.gc();

        System.out.print(w.get());
    }
}
