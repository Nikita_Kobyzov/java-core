package com.epam.lesson4.gc;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mikita_Kobyzau on 4/11/2016.
 */
public class Collector {
    private List<Object> list = new ArrayList<>();

    public void sayMemory(){
        System.out.println("Size is about " + list.size()*10 + "MB");
    }
    public void addMemory(){
        list.add(new byte[10*1024*1024]);
    }
    public void removeMemory(){
        list.removeAll(list);
    }
}
