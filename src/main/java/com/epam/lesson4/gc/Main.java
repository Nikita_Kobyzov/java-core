package com.epam.lesson4.gc;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by Mikita_Kobyzau on 4/11/2016.
 */
public class Main {
    private static boolean isWork = true;
    public static void main(String[] args) throws Exception{

        System.out.println("Starting...");
        Collector collector = new Collector();
        String in;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        while (isWork){
            collector.sayMemory();
            System.out.println("1 - add, 2 - delete, 0 - exit");
            in = bufferedReader.readLine();
            switch (in){
                case "0":
                    isWork = false;
                    break;
                case "1":
                    collector.addMemory();
                    break;
                case "2":
                    collector.removeMemory();
                    break;
            }
        }

    }

}
