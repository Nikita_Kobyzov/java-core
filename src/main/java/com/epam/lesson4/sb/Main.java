package com.epam.lesson4.sb;

/**
 * Created by Mikita_Kobyzau on 4/12/2016.
 */
public class Main {
    public static void main(String args[]) throws Exception{
        StringBuilder stringBuilder = new StringBuilder();
        StringBuffer stringBuffer = new StringBuffer();
        long before = System.currentTimeMillis();
        for(int i =0;i<1000000;i++){
            stringBuffer.append("Java");
        }
        long after = System.currentTimeMillis();
        System.out.println(after-before);
        before = System.currentTimeMillis();
        for(int i =0;i<1000000;i++){
            stringBuilder.append("Java");
        }
        after = System.currentTimeMillis();
        System.out.println(after-before);

        final StringBuilder stringBuilder1 = new StringBuilder();
        Thread t1 = new Thread(()->
        {
            for (int i = 0; i<1000; i++){
                stringBuilder1.append("a");
            }
        });
        Thread t2 = new Thread(()->
        {
            for (int i = 0; i<1000; i++){
                stringBuilder1.append("b");
            }
        });
        Thread t3 = new Thread(()->
        {
            for (int i = 0; i<1000; i++){
                stringBuilder1.append("c");
            }
        });


        t1.start();
        t2.start();
        t3.start();


        t1.join();
        t2.join();
        t3.join();


        System.out.println(stringBuilder1.toString());
        System.out.println(stringBuilder1.toString().length());
    }


}
